from django.db import models

class Category(models.Model):
    title = models.CharField(max_length=50, null=False, blank=False, verbose_name="Категория")
    description = models.TextField(max_length=3000, null=True, blank=True, verbose_name="Описание")

    def __str__(self):
        return f'#{self.pk} - {self.title}

class Product(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False, verbose_name="Продукт")
    description = models.TextField(max_length=3000, null=True, blank=True, verbose_name="Описание")
    category = models.ForeignKey(Category,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Время создания")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Время изменения")
    price = models.DecimalField(decimal_places=10, max_digits=0, null=False, blank=False, verbose_name="Стоимость")
    image_url = models.CharField(max_lenght=3000, null=False, blank=False, verbose_name="Изображение")

    def __str__(self):
        return f'#{self.pk} - {self.title}